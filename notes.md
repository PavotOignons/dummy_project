# How to delete multiple branches

## Le tri par date

Il est possible via la commande **git log** de récupérer toutes les actions concernant une branche à partir d'une telle date. Celle qui n'ont pas de log n'ont donc pas d'activité postérieure à cette date, et peuvent être considérée comme obsolète.
Le script prend trois paramêtres :
1. le nom du remote
2. La date ciblée (format AAAA-MM-JJ)
3. L'heure ciblée (format HH:MM:SS)
```shell
# Pour toutes les branches remote listées k (sauf celle qui contiennent une flèche ou un étoile)
for k in $(git branch -r | sed /\*/d | sed /-\>/d ); do 
    # si la branche courante k donne un log pour un commit fait avant la date donnée
    if [ -z "$(git log -1 --since="$2 $3" -s $k)" ]; then
        # on retire le nom du remote du nom de la branche
        branchName="$(echo $k | sed -r 's/'$1'\///')"
        # on supprime la branch dans le remote
        git push $1 --delete $branchName
    fi
done
```

## Le tri par nom
On tri sur une regex et on supprime celle qui match
On peut inverser la séléction avec l'attribut **-v** de grep
Par exemple on peut supprimer tout les branches fix/
```shell
git branch | grep 'fix\/' | xargs git branch -d
```
Ou supprimer toutes les branches relatives au ticket ayant un numéro antérieur à 1000 (ici sur SMC)
```shell
git branch | grep 'SMC-\d{4}' | xargs git branch -d
```
L'avantage c'est c'est aussi flexibles que les regex.

### Addendum - commandes git utiles
Mettre à jour les branches remote localement (en détruisant celle qui n'existent plus (sinon le script rale))
```shell
git remote update origin --prune
```
Faire en sorte que git retienne momentanément les infos de connexion (ici pour 1h). Sinon le scrpit peut demander de s'authentifier à chaque suppression.
```shell
git config --global credential.helper 'cache --timeout 3600'
```