const simpleGit = require('simple-git');

const date = "2019-10-31 16:00:00"

simpleGit().log({'-s': true, "--since": date, 'node-version': true}, (error, data) => {
    if(error){
        console.log('Error');
        console.log(error);
    } else {
        if(data.total > 0){
            console.log(data.latest.message);
        }
    }
});